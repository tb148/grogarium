#######################
Contributing guidelines
#######################

For anyone who knows `Git <https://git-scm.com>`__ and
`Python <https://www.python.org/>`__ basics: feel free to contribute to
this repository! Our code is open source. Be sure to follow our `Code of
Conduct <https://github.com/tb148/grogarium/blob/main/CODE_OF_CONDUCT.md>`__.

***************************************************************************
Filling `issues <https://docs.gitlab.com/ee/user/project/issues/#issues>`__
***************************************************************************

Found a bug? Report it! Be sure to check if it is already reported by
using the search bar
`here <https://gitlab.com/tb148/grogarium/-/issues>`__. If there are no
similar issues, you can create a new one. We will take a look at it.

Also, feel free to ask questions, suggest new features, and more.

**************************************************************************************************************
Creating `merge requests <https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests>`__
**************************************************************************************************************

Contributions are welcome! Be sure to file an issue first (see above).
If you know a fix for that issue, or you know a fix for a different one,
you can create a merge request. Fork this repository, create a new branch
from the 'main' branch, and make your changes on the new branch. Now,
create a merge request on the origin repository (tb148/grogarium). We
will review your merge request.

******************
Contributing rules
******************

-  Do one thing in a merge request. Open multiple merge requests if you
   want to add multiple things.
-  Please write `Conventional
   Commits <https://www.conventionalcommits.org/en/v1.0.0/>`__. So we
   can take track of what is added each commit. You can use
   `Commitizen <https://github.com/commitizen-tools/commitizen>`__ to
   write them.
-  Format your code using `Prettier <https://prettier.io/>`__ and
   `Black <https://black.readthedocs.io/en/stable/>`__. So we have a
   unified codestyle.
-  Don't remove features unless it's necessary. Add an option to disable
   some feature instead.
-  Keep the configuration file backward compatible all the time.
-  We use `Semantic Versioning <https://semver.org/>`__.
